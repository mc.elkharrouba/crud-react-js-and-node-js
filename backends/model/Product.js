const mongoose = require('mongoose');

const Product = new mongoose.Schema({
    ProductName : String,
    ProductDescription : String,
    ProductPrix:String,
    ProductCat:String,
    ProductImage:String
    
});

module.exports = mongoose.model('Product',Product);
