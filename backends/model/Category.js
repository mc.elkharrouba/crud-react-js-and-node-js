
const mongoose = require('mongoose');

const Category = new mongoose.Schema({
    CategoryName : String , 
    CategoryDescription : String 
});

module.exports = mongoose.model('Category',Category);