const express = require('express');
const app = express();
const mongoose = require('mongoose');
const Product = require('./model/Product');
const Category = require('./model/Category');
const bodyParser = require('body-parser');
const File =  require('./model/file');
const cors = require('cors');
const path = require("path");
const multer = require("multer");


app.use(cors());
app.use(express.static('uploads'));
app.use(bodyParser.json());

const storage = multer.diskStorage({
   destination: "./uploads/",
   filename: function(req, file, cb){
      cb(null,file.originalname);
   }
});

const upload = multer({
   storage: storage,
   limits:{fileSize: 1000000},
}).single("myfile");

const obj =(req,res) => {
    upload(req, res, () => {
       console.log("Request ---", req.body);
       console.log("Request file ---", req.file);
       const file = new File();
       file.meta_data = req.file;
       file.save().then(()=>{
       res.send({message:"uploaded successfully"})
       })
    });
 }

app.post("/upload", obj) ;
app.post('/ajout_cat',(req,res)=>{

    const cat = new Category(req.body);

    console.log(req.body);

    cat.save( (err,dep)=>{
        if(err){
            res.status(500).json(err);
        }
        else{
            res.status(201).json(dep);
        }
    });
});

app.post('/ajout_product',(req,res)=>{

    const obj = {
        ProductName : req.body.ProductName,
        ProductDescription : req.body.ProductDescription,
        ProductPrix : req.body.ProductPrix,
        ProductCat : req.body.ProductCat,
        ProductImage: req.body.ProductImage
    }
    const std = new Product(obj);

    console.log(req.body);

    std.save( (err,std)=>{
        if(err){
            res.status(500).json(err);
        }
        else{
            res.status(201).json(std);
        }
    });
});

//insert 
app.get('/get_cat',(req,res)=>{

    Category.find()
    .exec()
    .then(
        (respense) => {
            res.status(200).json(respense);
        }
    )

});

app.get('/get_product',(req,res)=>{

    Product.find()
    .exec()
    .then(
        (respense) => {
            res.status(200).json(respense);
        }
    )

});

app.get('/get_product_byId/:id',(req,res)=>{
    const id = req.params.id;
    Product.findById(id)
    .exec()
    .then(
        (respense) => {
            res.status(200).json(respense);
        }
    )

});

//Delete
app.delete('/delete_cat/:id',(req,res)=>{

    const id = req.params.id;
    Category.findByIdAndDelete(id, (err,response)=>{
        if(err)
        {
            res.status(500).json(err);
        }
        else
        {
           res.status(200).json(response._id);
        }
    });
});

app.delete('/delete_product/:id',(req,res)=>{

    const id = req.params.id;
    Product.findByIdAndDelete(id, (err,response)=>{
        if(err)
        {
            res.status(500).json(err);
        }
        else
        {
           res.status(200).json(response._id);
        }
    });
});


//udpate
app.post('/update_dep/:id',(req,res)=>{
    
    const id = req.params.id;

    Category.findById(id)
    .then( response =>{

        response.code = req.body.code,
        response.superviseur = req.body.superviseur,
        response.dep = req.body.dep;
        response.save()
        .then(result =>{
            res.json("save dep :" + result)

        })
        .catch( err=> console.log(err)) 
    })
    .catch(
        (err)=>{ console.log("err 1") ;}
    )
})


app.post('/update_product/:id',(req,res)=>{
    
    const id = req.params.id;

    Product.findById(id)
    .then( response =>{

        response.ProductName = req.body.ProductName,
        response.ProductDescription = req.body.ProductDescription,
        response.ProductPrix = req.body.ProductPrix,
        response.ProductCat  = req.body.ProductCat;
        response.save()
        .then(result =>{
            res.json("save dep :" + result)

        })
        .catch( err=> console.log(err)) 
    })
    .catch(
        (err)=>{ console.log(err) }
    )
})


;
mongoose.connect('mongodb+srv://owenvar:*******@cluster0.p54gs.mongodb.net/laboTP?retryWrites=true&w=majority',{ useNewUrlParser: true , useUnifiedTopology: true } );

app.listen(3154,()=>{
    console.log('connetcd to port 3154 !') ;
})



